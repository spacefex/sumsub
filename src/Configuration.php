<?php

namespace SumSub;

/**
 * Class Configuration
 * @package SumSub
 */
class Configuration
{
    private static $defaultConfiguration = null;

	/**
	 * Ibanq API secret
	 *
	 * @var string
	 */
    protected $secret = '';

    /**
     * Access token for OAuth.
     *
     * @var string
     */
    protected $token = '';

    /**
     * Username for HTTP basic authentication.
     *
     * @var string
     */
    protected $username = '';

    /**
     * Password for HTTP basic authentication.
     *
     * @var string
     */
    protected $password = '';

    /**
     * The default header(s).
     *
     * @var array
     */
    protected $defaultHeaders = [];

    /**
     * The host.
     *
     * @var string
     */
    protected $host = '';

    /**
     * The account level name.
     *
     * @var string
     */
    protected $levelName = '';

    /**
     * Debug switch (default set to false).
     *
     * @var bool
     */
    protected $debug = false;

    /**
     * Debug file location (log to STDOUT by default).
     *
     * @var string
     */
    protected $debugFile = 'php://output';

    /**
     * Debug file location (log to STDOUT by default).
     *
     * @var string
     */
    protected $tempFolderPath;

	/**
	 * Client id generated in ibanq
	 *
	 * @var  string
	 */
    protected $clientId;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tempFolderPath = sys_get_temp_dir();
    }

    public function setSecret(string $secret): self
	{
    	$this->secret = $secret;

    	return $this;
	}

	public function getSecret(): string
	{
		return $this->secret;
	}

    public function setToken(string $token): self
	{
		$this->token = $token;

		return $this;
	}

	public function getToken(): string
	{
		return $this->token;
	}

    public function setLevelName(string $levelName): self
	{
		$this->levelName = $levelName;

        return $this;
	}

    public function getLevelName(): string
	{
		return $this->levelName;
	}

    /**
     * Sets the username for HTTP basic authentication.
     *
     * @param string $username Username for HTTP basic authentication
     *
     * @return Configuration
     */
    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Gets the username for HTTP basic authentication.
     *
     * @return string Username for HTTP basic authentication
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the password for HTTP basic authentication.
     *
     * @param string $password Password for HTTP basic authentication
     *
     * @return Configuration
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the password for HTTP basic authentication.
     *
     * @return string Password for HTTP basic authentication
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Adds a default header.
     *
     * @param string $headerName  header name (e.g. Token)
     * @param string $headerValue header value (e.g. 1z8wp3)
     *
     * @return Configuration
     */
    public function addDefaultHeader($headerName, $headerValue)
    {
        if (!is_string($headerName)) {
            throw new \InvalidArgumentException('Header name must be a string.');
        }

        $this->defaultHeaders[$headerName] = $headerValue;

        return $this;
    }

    /**
     * Gets the default header.
     *
     * @return array An array of default header(s)
     */
    public function getDefaultHeaders()
    {
        return $this->defaultHeaders;
    }

    /**
     * Deletes a default header.
     *
     * @param string $headerName the header to delete
     *
     * @return Configuration
     */
    public function deleteDefaultHeader($headerName)
    {
        unset($this->defaultHeaders[$headerName]);
    }

    /**
     * Gets the host.
     *
     * @return string Host
     */
    public function setHost($host)
    {
        return $this->host = $host;
    }
    /**
     * Gets the host.
     *
     * @return string Host
     */
    public function getHost()
    {
        return $this->host;
    }
    /**
     * Sets debug flag.
     *
     * @param bool $debug Debug flag
     *
     * @return Configuration
     */
    public function setDebug($debug): self
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * Gets the debug flag.
     *
     * @return bool
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * Sets the debug file.
     *
     * @param string $debugFile Debug file
     *
     * @return Configuration
     */
    public function setDebugFile($debugFile): self
    {
        $this->debugFile = $debugFile;

        return $this;
    }

    /**
     * Gets the debug file.
     *
     * @return string
     */
    public function getDebugFile()
    {
        return $this->debugFile;
    }

    /**
     * Sets the temp folder path.
     *
     * @param string $tempFolderPath Temp folder path
     *
     * @return Configuration
     */
    public function setTempFolderPath($tempFolderPath): self
    {
        $this->tempFolderPath = $tempFolderPath;

        return $this;
    }

    /**
     * Gets the temp folder path.
     *
     * @return string Temp folder path
     */
    public function getTempFolderPath()
    {
        return $this->tempFolderPath;
    }

    /**
     * Gets the default configuration instance.
     *
     * @return Configuration
     */
    public static function getDefaultConfiguration()
    {
        if (self::$defaultConfiguration === null) {
            self::$defaultConfiguration = new self();
        }

        return self::$defaultConfiguration;
    }

    /**
     * Sets the detault configuration instance.
     *
     * @param Configuration $config An instance of the Configuration Object
     *
     * @return void
     */
    public static function setDefaultConfiguration(Configuration $config)
    {
        self::$defaultConfiguration = $config;
    }

    /**
     * Gets the essential information for debugging.
     *
     * @return string The report for debugging
     */
    public static function toDebugReport()
    {
        $report = 'PHP SDK (ModulrApiClient) Debug Report:'.PHP_EOL;
        $report .= '    OS: '.php_uname().PHP_EOL;
        $report .= '    PHP Version: '.phpversion().PHP_EOL;
        $report .= '    OpenAPI Spec Version: 1'.PHP_EOL;
        $report .= '    Temp Folder Path: '.self::getDefaultConfiguration()->getTempFolderPath().PHP_EOL;

        return $report;
    }
}
