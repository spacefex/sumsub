<?php

namespace SumSub;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * Class SumSubClient
 * @package SumSub
 */
class SumSubClient
{

    /**
     * @var Configuration $config
     */
    protected $config; 

    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $externalUserId
     * @param string $levelName
     *
     * @return object
     */
    public function createApplicant(string $externalUserId)
    {
        $requestBody = [
            'externalUserId' => $externalUserId
            ];
        $url = '/resources/applicants?levelName=' . $this->config->getLevelName();
        $request = new Psr7\Request('POST', $this->config->getHost() . $url);
        $request = $request->withHeader('Content-Type', 'application/json');
        $request = $request->withBody(Psr7\Utils::streamFor(json_encode($requestBody)));

        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
     * @param string $applicantId
     * @param array $metadata
     * @param string $filePath
     *
     * @return object
     */
    public function addDocument( string $applicantId, array $metadata, string $filePath)
    {
        // $metadata = ['idDocType' => 'PASSPORT', 'country' => 'GBR'];
        $multipart = new MultipartStream([
            [
                "name" => "metadata",
                "contents" => json_encode($metadata)
            ],
            [
                'name' => 'content',
                'contents' => fopen($filePath, 'r')
            ],
        ]);

        $url = "/resources/applicants/" . $applicantId . "/info/idDoc";
        $request = new Psr7\Request('POST', $this->config->getHost() . $url);
        $request = $request->withBody($multipart);

        return $this->sendHttpRequest($request, $url)->getHeader("X-Image-Id")[0];
    }

    /**
     * @param string $applicantId
     *
     * @return object
     */
    public function getApplicantStatus(string $applicantId)
    {
        $url = "/resources/applicants/" . $applicantId . "/requiredIdDocsStatus";
        $request = new Psr7\Request('GET', $this->config->getHost() . $url);

        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
     * @param string $applicantId
     *
     * @return object
     */
    public function getApplicantData(string $applicantId)
    {
        $url = "/resources/applicants/" . $applicantId . "/one";
        $request = new Psr7\Request('GET', $this->config->getHost() . $url);
        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
     * @param string $applicantId
     * @param string $reason
     *
     * @return object
     */
    public function applicantRechek(string $applicantId, string $reason)
    {
        $url = "/resources/applicants/" . $applicantId . "/status/pending?reason=".$reason;
        $request = new Psr7\Request('POST', $this->config->getHost() . $url);
        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
     * @param string $externalUserId
     * @param string $levelName
     *
     * @return object
     */
    public function getAccessToken(string $externalUserId, string $ttlInSecs = '900')
    {
        $url = "/resources/accessTokens?userId=" . $externalUserId .
                    "&levelName=" . $this->config->getLevelName(). 
                    "&ttlInSecs=" . $ttlInSecs;
        $request = new Psr7\Request('POST', $this->config->getHost() . $url);
        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
     * @param string $externalUserId
     * @param string $levelName
     *
     * @return object
     */
    public function getImage(string $inspectionId, string $imageId)
    {
        $url = "/resources/inspections/$inspectionId/resources/$imageId";
        $request = new Psr7\Request('GET', $this->config->getHost() . $url);
        
        return $this->sendHttpRequest($request, $url);
    }

    /**
     * @param string $applicantId
     *
     * @return object
     */
    public function setPendingStatus(string $applicantId)
    {
        // Requesting check for an applicant
        // Once all required documents are properly uploaded you can signal to us that an applicant
        // is ready to be reviewed by moving it to pending 
        // https://developers.sumsub.com/api-flow/#requesting-check-for-an-applicant
        $url = "/resources/applicants/" . $applicantId . "/status/pending";
        $request = new Psr7\Request('POST', $this->config->getHost() . $url);
        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
    * @param string $applicantId
    * @param array $params
    *
    * @return object
    */
    public function setTestStatus(string $applicantId, array $params)
    {
        // Testing on the test environment
        // Example: $params = [
        //     "reviewAnswer" => "RED" or "GREEN",
        //     "moderationComment" => "We do not accept screenshots. Please upload an original photo.",
        //     "clientComment" => "Screenshots are not accepted.",
        //     "reviewRejectType" => "RETRY" or "FINAL",
        //     "rejectLabels" => ["SCREENSHOTS"]
        // ];
        // More info: https://developers.sumsub.com/api-reference/#testing-on-the-test-environment
     
        $url = "/resources/applicants/" . $applicantId . "/status/testCompleted";
        $requestBody = $params;

        $request = new Psr7\Request('POST', $this->config->getHost() . $url);
        $request = $request->withHeader('Content-Type', 'application/json');
        $request = $request->withBody(Psr7\Utils::streamFor(json_encode($requestBody)));
        $request = $request->withBody(Psr7\Utils::streamFor(json_encode($requestBody)));

        return json_decode($this->sendHttpRequest($request, $url)->getBody());
    }

    /**
     * @param Request $request
     * @param string $url
     *
     * @return ResponseInterface
     */
    private function sendHttpRequest(Request $request, string $url): ResponseInterface
    {
        $client = new Client();
        $ts = time();
        $request = $request->withHeader('X-App-Token', $this->config->getToken());
        $request = $request->withHeader('X-App-Access-Sig', $this->createSignature($ts, $request->getMethod(), $url, $request->getBody()));
        $request = $request->withHeader('X-App-Access-Ts', $ts);
        // Reset stream offset to read body in `send` method from the start
        $request->getBody()->rewind();

        try {
            $response = $client->send($request);
            if ($response->getStatusCode() != 200 && $response->getStatusCode() != 201) {
                // https://developers.sumsub.com/api-reference/#errors
                throw new HttpResponseException(response(
                    $response->getBody()->getContents(),
                    Response::HTTP_UNPROCESSABLE_ENTITY
                ));
            }
        } catch (ClientException $e) {
            // catch default sumsub error's (json string format)
            // '{"description":"Invalid id 'tratata...'","code":400,"correlationId":"req-xxx-xxx-xxx"}'
            throw new HttpResponseException(response(
                $e->getResponse()->getBody()->getContents(),
                Response::HTTP_UNPROCESSABLE_ENTITY
            ));
        } catch (ConnectException $e) {
            // Catch fatal's like 'cURL error 6: Could not resolve host...' (string format)
            throw new HttpResponseException(response(
                $e->getMessage(),
                Response::HTTP_BAD_GATEWAY
            ));
        }

        return $response;
    }

    /**
     * @param string $ts
     * @param string $httpMethod
     * @param string $url
     * @param StreamInterface $httpBody
     *
     * @return string
     */
    private function createSignature(string $ts, string $httpMethod, string $url, StreamInterface $httpBody): string
    {
        return hash_hmac('sha256', $ts . strtoupper($httpMethod) . $url . $httpBody, $this->config->getSecret());
    }
}