<?php

namespace SumSub;

use Illuminate\Support\ServiceProvider;

/**
 * Class SumSubServiceProvider
 * @package SumSub
 */
class SumSubServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Determine if this is a Lumen application.
     *
     * @return bool
     */
    protected function isLumen(): bool
    {
        return str_contains($this->app->version(), 'Lumen');
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(): void
    {
        if (!$this->isLumen()) {
            $this->publishes([
                __DIR__.'/Config/sumsub.php' => config_path('sumsub.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(SumSubApi::class, function () {
            $api = new SumSubApi();
            $api->setApiPath(config('sumsub.api_url'))
            ->setSecret(config('sumsub.secret_app_token'))
            ->setToken(config('sumsub.appToken'))
            ->setDebugMode(config('sumsub.debug'));

            return $api;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [SumSubApi::class];
    }

}
