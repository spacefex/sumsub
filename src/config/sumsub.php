<?php

return [
	'secret' => config("sumsub.secret_app_token"),
	'appToken' => config("sumsub.token"),
	'api_url' => config("sumsub.url"),
	'debug' => config("sumsub.debug"),
	'level_name' => config("sumsub.level_name", "basic-kyc-level")
];
