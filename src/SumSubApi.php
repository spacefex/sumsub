<?php

namespace SumSub;

use Exception;

/**
 * Class SumSubApi
 * @package SumSub
 */
class SumSubApi
{
	/**
	 * @var SumSubClient;
	 */
	private $client;

	/**
	 * @var string
	 */
	private $apiPath;

	/**
	 * @var string
	 */
	private $secret;

	/**
	 * @var string
	 */
	private $token;

	/**
	 * @var string
	 */
	private $levelName;

	/**
	 * @var
	 */
	private $debugMode = false;

	/**
	 * SumSubApi constructor.
	 */
	public function __construct()
	{
		$this->apiPath = config("sumsub.url");
		$this->secret = config("sumsub.secret_app_token");
		$this->token = config("sumsub.token");
		$this->levelName = config("sumsub.level_name", "basic-kyc-level");
	}

	/**
	 * Sets API Path.
	 *
	 * @param string $apiPath
	 *
	 * @return $this
	 */
	public function setApiPath($apiPath)
	{
		$this->apiPath = $apiPath;

		return $this;
	}

	/**
	 * Sets API Path.
	 *
	 * @param string $apiKey
	 *
	 * @return $this
	 */
	public function setSecret(string $secret): self
	{
		$this->secret = $secret;

		return $this;
	}

	/**
	 * @param string $token
	 * @return SumSubApi
	 */
	public function setToken(string $token): self
	{
		$this->token = $token;

		return $this;
	}

	/**
	 * Sets Debug mode.
	 *
	 * @param string $debug
	 *
	 * @return $this
	 */
	public function setDebugMode($debug)
	{
		$this->debugMode = $debug;

		return $this;
	}

	/**
	 * Sets api level name.
	 *
	 * @param string $debug
	 *
	 * @return $this
	 */
	public function setLevelName($levelName)
	{
		$this->levelName = $levelName;

		return $this;
	}


	/**
	 * Checks if Ibanq config is setup.
	 *
	 * @throws ConfigException
	 */
	private function checkConfig()
	{
		if (!$this->secret) {
			//throw new \Exception('Please set your SumSub API key in the config file');
			return false;
		}

		return true;
	}

	/**
	 * @param  null $nonce
	 * @return SumSubClient
	 * @throws ConfigException
	 */
	private function createClient($nonce = null): ?SumSubClient
	{
		if (!$this->checkConfig()) {
			return null;
		}

		$config = new Configuration();

		$config->setSecret($this->secret);
		$config->setHost($this->apiPath);
		$config->setToken($this->token);
		$config->setLevelName($this->levelName);
		$config->setDebug($this->debugMode);

		$api = new SumSubClient($config);

		return $api;
	}

	public function getClient(): ?SumSubClient
	{
		if (!$this->client) {
			$this->client = $this->createClient();
		}

		return $this->client;
	}

}
