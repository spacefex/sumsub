SumSub module installation

- Add service provider into .../be/config/app.php (For Laravel v5.x only. Not need it for 8 version)
	- SumSub\SumSubServiceProvider::class,
- Add config key in .env
	- SUMSUB_SECRET=xxxx
	- SUMSUB_TOKEN=tst:xxx
	- SUMSUB_URL=https://test-api.sumsub.xxx
	- SUMSUB_DEBUG=false
	- SUMSUB_SECRET_KEY=vlbexxxxlx27xxxxxxsbn9lj7to (YOU CAN GET KEY FROM DASHBOARD ../checkus#/devSpace/webhooks)
	- SUMSUB_LEVEL_NAME=lev_name     # default value is basic-kyc-level
- Changes in composer,json
	- add in "require" section
		"spacefex/sumsub": "*"
	- prod/dev env add in "repositories" section (you must have bitbucket repo passwd)
		{
			"type": "vcs",
			"url": "git@bitbucket.org:spacefex/sumsub.git"
		}
	- local env for development add in "repositories" section
		{
			"type": "vcs",
			"url": "https://quatroit@bitbucket.org/spacefex/sumsub.git"
		}
- composer install | update
 